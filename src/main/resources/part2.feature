Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly


  Scenario Outline: Check site  verifies that user can submit a question to BBC
    Given User opens page
    And  User click on news page go to News
    And User click on Coronavirus tab
    And User click on Your Coronavirus Stories tab
    When User click button 'Submit'
    Then User checked '<errorMessageText>'
    And User checked '<errorMessageName>' Name
    And User checked '<errorMessageAccept>' Accepted

    Examples:
      | errorMessageText| errorMessageName     | errorMessageAccept     |
      |can't be blank   | Name can't be blank  | must be accepted  |




