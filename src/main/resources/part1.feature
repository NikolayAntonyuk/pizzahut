Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly


  Scenario Outline: Check site news page
    Given User opens page
    When User click on '<newsNumber>' news
    And User click on close pop up
    Then User the name of the '<headlineArticle>' against a value specified in your test

    Examples:
             | newsNumber | headlineArticle                                            |
             | 2          |Pakistan: Man accused of blasphemy killed by mob in Khanewal|


  Scenario Outline: Check site news secondary article titles
    Given User opens page
    When User click on '<newsNumber>' news
    And User click on pop up close
    Then User checks '<secondaryArticle>' on the List specified

    Examples:
     | newsNumber | secondaryArticle                                  |
     | 2          | Winter Olympics 2022: Day-by-day guide            |


  Scenario Outline: Check site Search bar
    Given User opens page
    When User click on '<newsNumber>' news
    And User click on pop up close
    And User stores the text of the Category link
    And User enter this text in the Search bar
    Then User checks the name of the first article against

    Examples:
            | newsNumber |
            | 1          |

