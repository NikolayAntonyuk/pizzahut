package manager;


import pages.FormPage;
import pages.HomePage;
import pages.NewsPage;
import pages.SearchPage;

public class PageFactoryManager {

    public static final HomePage homePage = new HomePage();
    public static final NewsPage newsPage = new NewsPage();
    public static final SearchPage searchPage = new SearchPage();
    public static final FormPage formPage = new FormPage();


}
