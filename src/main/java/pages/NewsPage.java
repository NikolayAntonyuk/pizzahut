package pages;

import manager.WebDriverSingleton;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class NewsPage extends BasePage {



    @FindBy(xpath = "//button[@class='tp-close tp-active']")
    private WebElement closePopUpRegister;

    @FindBy(xpath = "//h1[@id='main-heading']")
    private WebElement titleNews;

    @FindBy(xpath = "//p[@class='ssrcss-17zglt8-PromoHeadline e1f5wbog4']/span")
    private List<WebElement> titleMoreOnThisStoryNews;

    @FindBy(xpath = "//li[contains(@class, 'gs-o-list-ui__item--flush ge')]//span[contains(text(), 'Coronavirus')]")
    private WebElement buttonPageCoronavirus;

    @FindBy(xpath = "//a[@class='blue-tit__list-item-link']//span[contains(text(), 'Contact BBC News')]")
    private WebElement buttonPageContactBBCNews;

    @FindBy(xpath = "//p[@class='ssrcss-1q0x1qg-Paragraph eq5iqo00']//a[contains(text(), 'send us a story')]")
    private WebElement buttonIssueWithTheNews;



     public void NewsPage(final String url) {
        WebDriverSingleton.getDriver().get(url);
    }

    public void clickClosePopUpRegisterButton() {
        closePopUpRegister.click();
    }

    public void clickButtonPageCoronavirus() {
        buttonPageCoronavirus.click();
    }

    public void clickButtonIssueWithTheNews() {
        buttonIssueWithTheNews.click();
    }

    public void clickButtonPageContactBBCNews() {
        buttonPageContactBBCNews.click();
    }



    public WebElement isClosePopUpRegister() {
       return closePopUpRegister;
    }

    public String getTextNews() {
        return titleNews.getText();
    }



    public boolean getListSecondaryArticleText(String text) {
        for (WebElement we : titleMoreOnThisStoryNews) {
            if (we.getText().contains(text)) {
                return true;
            }
        }
        return false;
    }




}
