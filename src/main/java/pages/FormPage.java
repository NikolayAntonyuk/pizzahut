package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class FormPage {

    SelenideElement inputTellUsYourStory = $(byXpath("//div[@class='long-text-input-container']/textarea"));

    SelenideElement inputContactInfoName = $(byXpath("//input[@placeholder='Name']"));

    SelenideElement inputContactInfoEmail = $(byXpath("//input[@placeholder='Email address']"));

    SelenideElement inputContactInfoContactNumber = $(byXpath("//input[@placeholder='Contact number ']"));

    SelenideElement inputContactInfoContactLocation = $(byXpath("//input[@placeholder='Location ']"));

    ElementsCollection checkboxes = $$(byXpath("//div[@class='checkbox']//input"));

    SelenideElement buttonSubmit = $(byXpath("//div[@class='button-container']/button[@class='button']"));

    SelenideElement errorMessageCantBeBlank = $(byXpath("//div[@class='input-error-message' and contains(text(),'t be blank')]"));

    SelenideElement errorMessageNameCantBeBlank = $(byXpath("//div[@class='input-error-message' and contains(text(),'Name ')]"));

    SelenideElement errorMessageEmail = $(byXpath("//div[@class='input-error-message' and contains(text(),'Email address is invalid')]"));

    SelenideElement errorMessageMustBeAccepted = $(byXpath("//div[@class='input-error-message' and contains(text(),' must be accepted')]"));

    SelenideElement errorMessageMAX255CantBe = $(byXpath("//div[@class='input-error-message' and contains(text(),'maximum is 255 characters')]"));


    public void enterTextInputTellUsYourStory(final String storyText) {
        inputTellUsYourStory.clear();
        inputTellUsYourStory.sendKeys(storyText);
/*        getAndAttachScreenshot();
        logger.info(storyText + " - ok");*/
    }

    public void clickDontPublishMyName() {
        checkboxes.get(0).getText();
 //       logger.info(" ok");
    }


    public void clickAcceptTheTermsOfService() {
        checkboxes.get(1).click();
 //       logger.info(" ok");
    }


    public void clickSubmitButton() {
        buttonSubmit.click();
 //       logger.info(" ok");
    }


    public void isErrorMessageCantBeBlank() {
        errorMessageCantBeBlank.isDisplayed();
  //      logger.info(" ok");
    }


    public void isErrorMessageNameCantBeBlank() {
        errorMessageNameCantBeBlank.isDisplayed();
  //      logger.info(" ok");
    }


    public void isErrorMustBeAccepted() {
        errorMessageMustBeAccepted.isDisplayed();
  //      logger.info(" ok");
    }


    public void isErrorMessageMAX255CantBe() {
        errorMessageMAX255CantBe.isDisplayed();
  //      logger.info(" ok");
    }

    public void enterTextStory(String storyText) {
        inputTellUsYourStory.clear();
        inputTellUsYourStory.sendKeys(storyText);
/*        getAndAttachScreenshot();
        logger.info(storyText + " - ok");*/
    }


    public void enterTextName(String storyText) {
        inputContactInfoName.clear();
        inputContactInfoName.sendKeys(storyText);
 /*       getAndAttachScreenshot();
        logger.info(storyText + " - ok");*/
    }


    public void enterEmail(String storyText) {
        inputContactInfoEmail.clear();
        inputContactInfoEmail.sendKeys(storyText);
 /*       getAndAttachScreenshot();
        logger.info(storyText + " - ok");*/
    }


    public WebElement isTextErrorMessageMAX255CantBe() {
        return errorMessageMAX255CantBe;

    }


    public String getErrorMessageCantBeBlank() {
        return errorMessageCantBeBlank.getText();

    }


    public String getErrorMessageNameCantBeBlank() {
        return errorMessageNameCantBeBlank.getText();
    }

    public String getErrorMessageEmail() {
        return errorMessageEmail.getText();
    }


    public String getErrorMessageMAX255CantBe() {
        return errorMessageMAX255CantBe.getText();
    }


    public String getErrorMessageMustBeAccepted() {
        return errorMessageMustBeAccepted.getText();
    }


    public WebElement getElementErrorMessageCantBeBlank() {
        return errorMessageCantBeBlank;
    }


    public WebElement getElementErrorMessageNameCantBeBlank() {
        return errorMessageNameCantBeBlank;
    }


}
