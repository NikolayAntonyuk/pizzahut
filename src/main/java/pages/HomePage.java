package pages;

import manager.WebDriverSingleton;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {



    @FindBy(xpath = "//div[@id='orb-header']")
    private WebElement header;

    @FindBy(xpath = "//a[@id='homepage-link']")
    private WebElement logoHomePage;

    @FindBy(xpath = "//section[contains(@class, 'module module--news')]//a[@class='media__link']")
    private List<WebElement> listNews;

    @FindBy(xpath = "//span[@class='ssrcss-1hhm2vt-SearchText eki2hvo2']")
    private WebElement buttonSearch;

    @FindBy(xpath = "//div[@id='orb-nav-links']//li[@class='orb-nav-newsdotcom']")
    private WebElement buttonPageNews;



    public void openHomePage(final String url) {
        WebDriverSingleton.getDriver().get(url);
    }



    public void isHeaderVisible() {
        header.isDisplayed();
    }


    public void clickButtonSearch() {
        buttonSearch.click();
    }

    public void clickButtonPageNews() {
        buttonPageNews.click();
    }


    public void clickListNews(String newsNumber) {
         listNews.get(Integer.parseInt(newsNumber)).click();
    }


}
