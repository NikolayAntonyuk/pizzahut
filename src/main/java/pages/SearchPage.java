package pages;

import manager.WebDriverSingleton;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchPage extends BasePage {


    @FindBy(xpath = "//input[@id='search-input']")
    private WebElement searchInput;

    @FindBy(xpath = "//span[@data-testid='actions:search']")
    private WebElement searchButton;

    @FindBy(xpath = "//p[@class='ssrcss-6arcww-PromoHeadline e1f5wbog4']/span")
    private List<WebElement> searchResultsTitles;



    public void SearchPage(final String url) {
        WebDriverSingleton.getDriver().get(url);
    }


    public void enterTextToSearchField(final String searchText) {
        searchInput.clear();
        searchInput.sendKeys(searchText);
    }


    public void clickSearchButton() {
        searchButton.click();
    }


    public String getSearchTitleContains(int newsNumber) {
        return searchResultsTitles.get(newsNumber).getText();
    }






}
