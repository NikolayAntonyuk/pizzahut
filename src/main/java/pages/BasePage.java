package pages;

import manager.WebDriverSingleton;
import org.openqa.selenium.support.PageFactory;

public class BasePage {


    public BasePage() {
        PageFactory.initElements(WebDriverSingleton.getDriver(), this);
    }
}
