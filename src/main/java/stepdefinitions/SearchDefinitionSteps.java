package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import static manager.PageFactoryManager.*;
import static org.junit.Assert.assertEquals;




public class SearchDefinitionSteps {


    private static final long DEFAULT_TIMEOUT = 30;
    private static String bufferText;



    @And("User enter this text in the Search bar")
    public void userEnterThisTextInTheSearchBar() {
        bufferText = newsPage.getTextNews();
        homePage.clickButtonSearch();
        searchPage.enterTextToSearchField(bufferText);
        searchPage.clickSearchButton();

    }

    @Then("User checks the name of the first article against")
    public void userChecksTheNameOfTheFirstArticleAgainst() {
        assertEquals(searchPage.getSearchTitleContains(0), bufferText);
    }


}
