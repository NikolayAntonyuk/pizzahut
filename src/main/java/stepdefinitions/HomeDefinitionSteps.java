package stepdefinitions;


import data.Projects;
import io.cucumber.java.en.And;

import static manager.PageFactoryManager.homePage;
import static manager.WebDriverSingleton.getDriver;
import static org.junit.Assert.assertTrue;

public class HomeDefinitionSteps {

    @And("User opens page")
    public void openPage() {
        homePage.openHomePage(Projects.BBC_MAIN_PAGE.getUrl());
    }

    @And("User checks header visibility")
    public void checkHeaderVisibility() {
        homePage.isHeaderVisible();
    }


    @And("User checks that current url contains {string} language")
    public void checkCurrentUrl(String language) {
         assertTrue(getDriver().getCurrentUrl().contains(language));
    }


    @And("User click on {string} news")
    public void userClickOnQuantityNews(String newsNumber) {
      homePage.clickListNews(newsNumber);
    }

    @And("User click on news page go to News")
    public void userClickOnNewsPageGoToNews() {
        homePage.clickButtonPageNews();
    }


}
