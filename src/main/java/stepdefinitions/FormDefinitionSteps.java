package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static manager.PageFactoryManager.*;
import static org.junit.Assert.assertEquals;

public class FormDefinitionSteps {

    @When("User click button 'Submit'")
    public void userClickButtonSubmit() {
        formPage.clickSubmitButton();
    }


    @Then("User checked {string}")
    public void userCheckedErrorMessageText(String errorMessageText) {
        assertEquals(formPage.getErrorMessageCantBeBlank(), errorMessageText);
        }
}
