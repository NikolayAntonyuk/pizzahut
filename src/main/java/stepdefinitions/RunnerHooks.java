package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import manager.WebDriverSingleton;

public class RunnerHooks {
    @Before
    public void testsSetUp() {
        WebDriverSingleton.getDriver();
    }



    @After
    public void tearDown() {
        WebDriverSingleton.quitDriver();
    }
}
