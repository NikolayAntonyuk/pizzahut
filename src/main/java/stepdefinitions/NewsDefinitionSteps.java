package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static manager.PageFactoryManager.newsPage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class NewsDefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 30;
    private static String bufferText;


    @And("User click on close pop up")
    public void userClickOnClosePopUp() {
        newsPage.isClosePopUpRegister();
        newsPage.clickClosePopUpRegisterButton();
    }

    @And("User the name of the {string} against a value specified in your test")
    public void userTheNameOfTheHeadlineArticleAgainstAValueSpecifiedInYourTest(String headlineArticle) {
        assertEquals(newsPage.getTextNews(), headlineArticle);
    }

    @And("User click on pop up close")
    public void userClickOnPopUpClose() {
       // newsPage.isClosePopUpRegister();
     //   newsPage.clickClosePopUpRegisterButton();
    }


    @Then("User checks {string} on the List specified")
    public void userChecksSecondaryArticleOnTheListSpecified(String text) {
        assertTrue(newsPage.getListSecondaryArticleText(text));
    }

    @And("User stores the text of the Category link")
    public void userStoresTheTextOfTheCategoryLink() {
      //  bufferText = newsPage.getTextNews();
    }


    @And("User click on Coronavirus tab")
    public void userClickOnTab() {
         newsPage.clickButtonPageCoronavirus();
    }

    @And("User click on Your Coronavirus Stories tab")
    public void userClickOnYourCoronavirusStoriesTab() {
        newsPage.isClosePopUpRegister();
        newsPage.clickClosePopUpRegisterButton();
        newsPage.clickButtonPageContactBBCNews();
        newsPage.clickButtonIssueWithTheNews();
    }

    @When("User click 'Contact BBC News'")
    public void userClickContactBBCNews() {
        newsPage.clickButtonPageContactBBCNews();
    }

    @And("User click 'send us a story'")
    public void userClickSendUsAStory() {
        newsPage.clickButtonIssueWithTheNews();
    }
}
