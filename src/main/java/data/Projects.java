package data;

public enum Projects {
    BBC_MAIN_PAGE("https://www.bbc.com/"),
    BBC_NEWS_PAGE("https://www.bbc.com/news"),
    BBC_CORON_PAGE("https://www.bbc.com/news/coronavirus");

    private final String value;

    Projects(String value) {
        this.value = value;
    }

    public String getUrl() {
        return value;
    }
}
