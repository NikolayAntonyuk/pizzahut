package runner;

import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/main/resources",
    glue = "stepdefinitions",
       // dryRun = true
       // tags = @first
        publish = true
)
public class RunnerTests {

}
